from flask import render_template, url_for, flash, redirect, request
from proiect import app
import mysql.connector, json

app.jinja_env.globals.update(json=json)

mydb = mysql.connector.connect(
                                host="localhost",
                                user="root",
                                passwd="testing321",
                                auth_plugin='mysql_native_password'
                            )

cursor = mydb.cursor()

def check_not_null(table):

    ok = False
    pk_list = []

    for column in table:
        if column['column_type'] == 'PRI':
            pk_list.append(column)

    if len(pk_list) != len(table):
        for column in table:
            if column['column_type'] != 'PRI' and column['is_null'] == 'NO':
                ok = True
    else:
        ok = True

    return ok
        
def check_pk(table):

    # check if pk:
    # status 0 - nu exista pk
    # status 1 - are pk dar nu e numeric
    # status 2 - are pk numeric pe mai multe coloane
    # status 3 - pk este ok

    pk_list = []

    for column in table:
        if column['column_type'] == 'PRI':
            pk_list.append(column)

    if len(pk_list) == 0:
        return {'table_name': table[0]['table_name'], 'status':0}

    else:

        if len(pk_list) == 1:

            if pk_list[0]['data_type'] == 'int':
                return {'table_name': table[0]['table_name'], 'status': 3}
            else:
                return {'table_name': table[0]['table_name'], 'status': 1}

        else:
            return {'table_name': table[0]['table_name'], 'status': 2}

@app.route("/")
@app.route("/db-scanner", methods = ['GET', 'POST'])
def db_scanner():

    if request.method == 'POST':

        table_columns = []
        integrity_pk = []
        integrity_nn = []

        for tb in request.form.getlist('tbs'):
            
            listt = []

            cursor.execute(f''' SELECT COLUMN_NAME, COLUMN_KEY,IS_NULLABLE, DATA_TYPE
                                FROM INFORMATION_SCHEMA.COLUMNS
                                WHERE TABLE_SCHEMA = '{request.form['dbs']}'
                                AND TABLE_NAME = '{tb}' ''')

            for x in cursor:
                table = {}
                table['table_name'] = tb
                table['column_name'] = str(x[0])
                table['column_type'] = str(x[1].decode())
                table['is_null'] = str(x[2])
                table['data_type'] = str(x[3].decode())
                listt.append(table)

            if check_pk(listt)['status'] != 3: integrity_pk.append(check_pk(listt))

            table_columns.append(listt)

        for table in table_columns:

            nns = check_not_null(table)
            if not nns:
                integrity_nn.append(table[0]["table_name"])

        return render_template('result.html', values = table_columns, pk = integrity_pk if integrity_pk else '', nn = integrity_nn if integrity_nn else '', db = request.form['dbs'])

    cursor.execute('Show databases')
    default_db = ['information_schema', 'mysql', 'performance_schema', 'sys']
    available_db = []

    for x in cursor:
        if str(x[0].decode()) not in default_db:
            available_db.append(str(x[0].decode()))

    return render_template('db-scanner.html', av_db = available_db)

@app.route("/result", methods = ['GET', 'POST'])
def result():
    print(request.form)
    return render_template('result.html')

@app.route('/get-tables', methods = ['GET', 'POST'])
def get_tables():
    cursor.execute(f""" SELECT table_name 
                        FROM information_schema.tables 
                        WHERE table_schema = '{request.form['db']}'
                        AND Table_Type = 'BASE TABLE' """)
    available_tables = []
    for x in cursor:
        available_tables.append(str(x[0].decode()))
    at = tuple(available_tables)
    return {'available_tables': at}

@app.route('/get-table-content', methods = ['GET', 'POST'])
def get_table_content():

    table = request.form['tb']
    database = request.form['db']
    columns = []

    cursor.execute(f''' SELECT COLUMN_NAME, IS_NULLABLE
                        FROM INFORMATION_SCHEMA.COLUMNS
                        WHERE TABLE_SCHEMA = '{database}' 
                        AND TABLE_NAME = '{table}'
                        AND COLUMN_KEY != 'PRI' 
                        '''
                    )

    for col in cursor:
        if col[1] == 'YES':
            columns.append(col[0])

    return {'columns': columns}

@app.route('/apply-nn', methods = ['GET', 'POST'])
def apply_nn():

    database = request.form['db']
    table    = request.form['tb']
    columns  = []

    columns.extend(request.form.getlist('columns[]'))

    for column in columns:
        cursor.execute(f''' SELECT DATA_TYPE, CHARACTER_MAXIMUM_LENGTH 
                            FROM INFORMATION_SCHEMA.COLUMNS 
                            WHERE TABLE_NAME = '{table}'
                            AND TABLE_SCHEMA = '{database}'
                            AND COLUMN_NAME = '{column}' 
                            ''')
        for dt in cursor:
            data_type = dt[0].decode()
            max_length = dt[1]

        cursor.execute(f''' UPDATE {database}.{table}
                            SET {column} = ''
                            WHERE {column} IS NULL; ''')

        cursor.execute(f''' ALTER TABLE {database}.{table} MODIFY {column} {data_type}{ f'({max_length})' if max_length != None else '' } NOT NULL ''')

    return {'msg': 'success'}

@app.route('/apply-pk', methods = ['GET', 'POST'])
def apply_pk():
    
    database    = request.form['db']
    table       = request.form['tb']
    pk_name     = request.form['pk']
    status      = int(request.form['status'])

    if status == 0:

        cursor.execute(f''' ALTER TABLE {database}.{table} 
                            ADD {pk_name} INT AUTO_INCREMENT PRIMARY KEY; ''')

        return {'msg': 'success'}

    elif status == 1:

        cursor.execute(f''' SELECT COLUMN_NAME
                            FROM INFORMATION_SCHEMA.COLUMNS
                            WHERE TABLE_SCHEMA = '{database}' 
                            AND TABLE_NAME = '{table}'
                            AND COLUMN_KEY = 'PRI'  '''
                    )

        for x in cursor:
            actual_pk = x[0]

        cursor.execute(f''' SELECT TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME
                            FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                            WHERE REFERENCED_TABLE_SCHEMA = '{database}' 
                            AND REFERENCED_TABLE_NAME = '{table}' 
                            AND REFERENCED_COLUMN_NAME = '{actual_pk}'; ''')

        foreign_keys = cursor.fetchall()

        if foreign_keys:

            fk_list = []

            for x in foreign_keys:
                fk_list.append(
                        {
                            'table_name':       str(x[0].decode()),
                            'table_column':     str(x[1]),
                            'constraint_name':  str(x[2]),
                            'ref_table':        str(x[3].decode()),
                            'ref_column':       str(x[4])
                        }
                    )
            
            for fk in fk_list:

                rec_list = []

                # 1st step: delete old foreign key

                cursor.execute(f''' ALTER TABLE {database}.{fk['table_name']} DROP FOREIGN KEY {fk['constraint_name']} ''')

                # 2nd step: delete old primary key constraint

                cursor.execute(f''' ALTER TABLE {database}.{table} DROP PRIMARY KEY ''')

                # 3rd step: add autonumber column               

                cursor.execute(f''' ALTER TABLE {database}.{table} ADD {pk_name} INT AUTO_INCREMENT PRIMARY KEY ''')
                
                # 4th step: new int column in referenced tables               

                cursor.execute(f''' ALTER TABLE {database}.{fk['table_name']} ADD {pk_name} INT NOT NULL ''')

                # 5th step: get name of the primary key column of the referenced table

                cursor.execute(f''' SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '{database}' AND TABLE_NAME = '{fk['table_name']}' AND COLUMN_KEY = 'PRI' ''')

                id_column = cursor.fetchone()

                id_column  = id_column[0]

                # 6th step: get all records 

                cursor.execute(f''' SELECT  {id_column}, {database}.{table}.{pk_name}
                                    FROM {database}.{table}
                                    RIGHT JOIN {database}.{fk['table_name']} ON {database}.{table}.{fk['ref_column']} = {database}.{fk['table_name']}.{fk['table_column']} 
                                    ''')

                ids = cursor.fetchall()

                # 7th step: update all records with the new foreign key 

                for i in ids:
                    record = {}
                    record['ref_table'] = str(i[0])
                    record['initial_table'] = str(i[1])
                    rec_list.append(record)
                
                for record in rec_list:
                    cursor.execute(f''' UPDATE {database}.{fk['table_name']}
                                        SET {database}.{fk['table_name']}.{pk_name} = {record['initial_table']}
                                        WHERE {database}.{fk['table_name']}.{id_column} = {record['ref_table']} ''')
                
                mydb.commit()

                # 8th step: drop old foreign key column

                cursor.execute(f''' ALTER TABLE {database}.{fk['table_name']} DROP COLUMN {fk['table_column']} ''')

                # 9th step: add new foreign key constraint

                cursor.execute(f''' ALTER TABLE {database}.{fk['table_name']}
                                    ADD FOREIGN KEY ({pk_name}) REFERENCES {database}.{table}({pk_name}) ''')

                return {'msg': 'success'}

        else:
            cursor.execute(f''' ALTER TABLE {database}.{table} DROP PRIMARY KEY ''')
            cursor.execute(f''' ALTER TABLE {database}.{table} ADD {pk_name} INT AUTO_INCREMENT PRIMARY KEY ''')

    elif status == 2:

        primary_keys = []

        cursor.execute(f''' SELECT COLUMN_NAME
                            FROM INFORMATION_SCHEMA.COLUMNS
                            WHERE TABLE_SCHEMA = '{database}' 
                            AND TABLE_NAME = '{table}'
                            AND COLUMN_KEY = 'PRI'  '''
                    )

        for x in cursor:
            primary_keys.append(str(x[0]))

        fk_pk = []

        for pk in primary_keys:

            cursor.execute(f''' SELECT TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME
                                FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                                WHERE REFERENCED_TABLE_SCHEMA = '{database}' 
                                AND TABLE_NAME = '{table}' 
                                AND COLUMN_NAME = '{pk}'; ''')

            foreign_keys = cursor.fetchall()

            for x in foreign_keys:
                fk_pk.append(
                                { 
                                    'table_name':       str(x[0].decode()),
                                    'table_column':     str(x[1]),
                                    'constraint_name':  str(x[2]),
                                    'ref_table':        str(x[3].decode()),
                                    'ref_column':       str(x[4])
                                }
                            )

        for fk in fk_pk:
                    
            try:

                mydb.autocommit = False

                cursor.execute(f'''ALTER TABLE {database}.{table} DROP FOREIGN KEY {fk['constraint_name']}''')

                mydb.commit()

            except mysql.connector.Error as error:
                print(error)
                mydb.rollback()

            mydb.autocommit = True

        cursor.execute(f''' ALTER TABLE {database}.{table} DROP PRIMARY KEY ''')

        for fk in fk_pk:

            try:

                mydb.autocommit = False

                cursor.execute(f''' ALTER TABLE {database}.{fk['table_name']}
                                    ADD FOREIGN KEY ({fk['table_column']}) REFERENCES {database}.{fk['ref_table']}({fk['table_column']})''')

                mydb.commit()

            except mysql.connector.Error as error:
                print(error)
                mydb.rollback()

            mydb.autocommit = True

        cursor.execute(f''' ALTER TABLE {database}.{table} ADD {pk_name} INT AUTO_INCREMENT PRIMARY KEY ''')

        return {'msg': 'success'}